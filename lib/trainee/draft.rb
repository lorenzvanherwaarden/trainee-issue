class Draft
  FILE_PATH = File.expand_path("../../draft/draft.md", __dir__)

  def entries
    File.read(FILE_PATH).split("#{Template::SEPARATOR}\n")
  end

  def update(merge_request, changes)
    File.open(FILE_PATH, "a") { |file| file.write(Template.new(merge_request, changes).call) }
  end

  def delete
    File.truncate(FILE_PATH, 0)
  end
end
